from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter
from mission.api.views import MissionConfigViewSet, RoverViewSet

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("mission-config", MissionConfigViewSet)
router.register("rover", RoverViewSet)

app_name = "api"
urlpatterns = router.urls
