from django.contrib import admin
from django.urls import path, include

from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
    openapi.Info(
        title="Eventmobi Mars API",
        default_version='v1',
        description="Eventmobi Mars API",
    ),
    public=True,
)

urlpatterns = [
    path('admin/', admin.site.urls),
]

urlpatterns += [
    path("api/docs/", schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    # API base url
    path("api/v1/", include("eventmobi.api_router")),
]
