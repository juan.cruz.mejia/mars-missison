from rest_framework import viewsets, mixins
from .serializers import MissionConfigSerializer, RoverSerializer
from ..models import MissionConfig, Rover


class MissionConfigViewSet(viewsets.ModelViewSet):
    queryset = MissionConfig.objects.all()
    serializer_class = MissionConfigSerializer


class RoverViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet):
    queryset = Rover.objects.all().order_by('order')
    serializer_class = RoverSerializer
    filterset_fields = ['mission']
