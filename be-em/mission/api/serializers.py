from rest_framework import serializers
from ..models import MissionConfig, Rover
from ..utils import new_position


class MissionConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = MissionConfig
        fields = ['id', 'mission_name', 'plateau_size_x', 'plateau_size_y', 'rovers_number']
        read_only_fields = ['id']


class MissionConfigReadOnlySerializer(serializers.ModelSerializer):
    class Meta:
        model = MissionConfig
        fields = ['id', 'mission_name']
        read_only_fields = fields


class RoverSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rover
        fields = ['id', 'mission', 'name', 'x_position', 'y_position', 'direction', 'instructions', 'order']
        read_only_fields = ['id', 'mission', 'order']

    def validate_instructions(self, attrs):
        for letter in attrs.upper():
            if letter not in ["L", "R", "M"]:
                raise serializers.ValidationError("Instructions must only contain L R or M")
        return attrs

    def update(self, instance, validated_data):
        current_position = {
            'x': validated_data.get('x_position'),
            'y': validated_data.get('y_position'),
            'direction': validated_data.get('direction'),
        }

        instructions = validated_data.get('instructions').upper()

        final_position = new_position(instructions, current_position)

        instance.name = validated_data.get('name')
        instance.x_position = final_position.get('x')
        instance.y_position = final_position.get('y')
        instance.direction = final_position.get('direction')
        instance.instructions = instructions
        instance.save()
        return instance

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['mission'] = MissionConfigReadOnlySerializer(instance.mission).data
        return response
