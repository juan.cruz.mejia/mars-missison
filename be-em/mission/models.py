import uuid

from django.db import models


class TimeStampedModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class MissionConfig(TimeStampedModel):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)
    mission_name = models.CharField(max_length=250, unique=True)
    plateau_size_x = models.PositiveIntegerField()
    plateau_size_y = models.PositiveIntegerField()
    rovers_number = models.PositiveIntegerField()

    class Meta:
        verbose_name = 'Mission Config'
        verbose_name_plural = 'Missions Config'

    def __str__(self):
        return f'Mission name: {self.mission_name}'


class Rover(TimeStampedModel):
    class CardinalPoints(models.TextChoices):
        NORTH = 'N', 'North'
        EAST = 'E', 'East'
        SOUTH = 'S', 'South'
        WEST = 'W', 'West'

    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)
    mission = models.ForeignKey(MissionConfig, on_delete=models.CASCADE, related_name="rover_mission")
    name = models.CharField(max_length=250, unique=True, null=True)
    x_position = models.IntegerField(null=True)
    y_position = models.IntegerField(null=True)
    direction = models.CharField(max_length=1, choices=CardinalPoints.choices, null=True)
    instructions = models.CharField(max_length=250, null=True)
    order = models.PositiveSmallIntegerField()

    class Meta:
        verbose_name = 'Rover'
        verbose_name_plural = 'Rovers'

    def __str__(self):
        return f'Rover name: {self.name}'

