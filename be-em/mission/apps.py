from django.apps import AppConfig


class MissionConfig(AppConfig):
    name = 'mission'

    def ready(self):
        try:
            import mission.signals
        except ImportError as e:
            print(e)
            pass
