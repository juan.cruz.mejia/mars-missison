
def new_position(instructions, current_position):
    for instruction in instructions:
        if instruction == 'L':
            if current_position['direction'] == 'N':
                current_position['direction'] = 'W'

            elif current_position['direction'] == 'E':
                current_position['direction'] = 'N'

            elif current_position['direction'] == 'S':
                current_position['direction'] = 'E'

            elif current_position['direction'] == 'W':
                current_position['direction'] = 'S'

        elif instruction == 'R':
            if current_position['direction'] == 'N':
                current_position['direction'] = 'E'

            elif current_position['direction'] == 'E':
                current_position['direction'] = 'S'

            elif current_position['direction'] == 'S':
                current_position['direction'] = 'W'

            elif current_position['direction'] == 'W':
                current_position['direction'] = 'N'

        elif instruction == 'M':
            if current_position['direction'] == "N":
                current_position['y'] = current_position['y'] + 1

            elif current_position['direction'] == "E":
                current_position['x'] = current_position['x'] + 1

            elif current_position['direction'] == "S":
                current_position['y'] = current_position['y'] - 1

            elif current_position['direction'] == "W":
                current_position['x'] = current_position['x'] - 1

    return current_position
