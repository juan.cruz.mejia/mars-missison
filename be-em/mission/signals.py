from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import MissionConfig, Rover


@receiver(post_save, sender=MissionConfig)
def create_mission_rovers(sender, instance, created, **kwargs):
    if created:
        for number in range(instance.rovers_number):
            Rover.objects.create(mission=instance, order=number)