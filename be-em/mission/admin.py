from django.contrib import admin
from .models import MissionConfig, Rover

admin.site.register(MissionConfig)
admin.site.register(Rover)
