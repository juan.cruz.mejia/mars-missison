## Mars Mission

### Prerequisites
+ docker >= 20.10.14
+ docker-compose >= 1.29.2

### Clone the repository

+ Open a terminal in the root of the repository run the following commands
  ```
  docker-compose build 
  docker-compose run --rm django python manage.py migrate
  docker-compose run --rm django python manage.py createsuperuser
  docker-compose up
  ```

### General functionality
  + Go to you [localhost](http://localhost:3000/)
  + Anytime you can click in the logo to go to the [main menu](http://localhost:3000/)
  + You will be able to see the menu items:
    + In the New Mission option you would be able to set up the initial configuration.
    + After you submit the form, you will be redirected to the mission details, there you can set the Rovers Information
    + When you fill the Rovers information and click on the save button, you will get the changes immediately in the same component
      + _Note: If you click on save again, the instructions will be performed with the current position and direction_

### Backend
+ You can log in to the [django admin](http://localhost:8000/admin) with the superuser credentials you registered before

