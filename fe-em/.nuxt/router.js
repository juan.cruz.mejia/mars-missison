import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _05452a60 = () => interopDefault(import('../pages/load.vue' /* webpackChunkName: "pages/load" */))
const _4940a3a6 = () => interopDefault(import('../pages/new.vue' /* webpackChunkName: "pages/new" */))
const _fa29113e = () => interopDefault(import('../pages/previous.vue' /* webpackChunkName: "pages/previous" */))
const _2dfb1658 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))
const _11ee9cdc = () => interopDefault(import('../pages/_idMission.vue' /* webpackChunkName: "pages/_idMission" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/load",
    component: _05452a60,
    name: "load"
  }, {
    path: "/new",
    component: _4940a3a6,
    name: "new"
  }, {
    path: "/previous",
    component: _fa29113e,
    name: "previous"
  }, {
    path: "/",
    component: _2dfb1658,
    name: "index"
  }, {
    path: "/:idMission",
    component: _11ee9cdc,
    name: "idMission"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
